#ifndef PETER_KUTAK_DISPLAY_DISPLAYI2C_H
#define PETER_KUTAK_DISPLAY_DISPLAYI2C_H

#include <ti/drivers/dpl/SemaphoreP.h>
#include <ti/drivers/I2C.h>
#include <ti/display/Display.h>
#include <stdint.h>

extern const Display_FxnTable DisplayI2c_fxnTable;

typedef struct DisplayI2c_HWAttrs
{
    /** Index of i2c in I2C_config[] */
    unsigned int i2cIdx;
    /** Address of LCD on I2C bus */
    unsigned int address;
    /** Timeout for acquiring semaphore */
    unsigned int mutexTimeout;
    /** Buffer for formatting messages */
    char *strBuf;
    /** Size of buffer */
    uint16_t strBufLen;
    /** Number of lines */
    uint8_t numLines;
    /** Backlight setting */
    uint8_t backlight;
    /** Cursor setting */
    uint8_t cursorOn;
    /** Cursor blinking */
    uint8_t cursorBlink;
} DisplayI2c_HWAttrs;

typedef struct DisplayI2c_Object
{
    I2C_Handle hI2c;
    SemaphoreP_Handle mutex;
} DisplayI2c_Object, *DisplayI2c_Handle;

void DisplayI2c_init(Display_Handle handle);

Display_Handle DisplayI2c_open(Display_Handle, Display_Params * params);

void DisplayI2c_clear(Display_Handle handle);

void DisplayI2c_clearLines(Display_Handle handle, uint8_t fromLine,
                           uint8_t toLine);

void DisplayI2c_vprintf(Display_Handle handle, uint8_t line, uint8_t column,
                        char *fmt, va_list va);

void DisplayI2c_close(Display_Handle);

int DisplayI2c_control(Display_Handle handle, unsigned int cmd, void *arg);

unsigned int DisplayI2c_getType(void);

#endif
