#include <unistd.h>
#include <string.h>
#include <ti/drivers/I2C.h>

#include <ti/display/Display.h>
#include "DisplayI2c.h"

/*
 * By default disable both asserts and log for this module.
 * This must be done before DebugP.h is included.
 */
#ifndef DebugP_ASSERT_ENABLED
#define DebugP_ASSERT_ENABLED 0
#endif
#ifndef DebugP_LOG_ENABLED
#define DebugP_LOG_ENABLED 0
#endif

#include <ti/drivers/dpl/DebugP.h>
#include <ti/drivers/dpl/SemaphoreP.h>
#include <ti/drivers/dpl/SystemP.h>

/** Display function table for I2C implementation */
const Display_FxnTable DisplayI2c_fxnTable = { DisplayI2c_init, DisplayI2c_open,
                                               DisplayI2c_clear,
                                               DisplayI2c_clearLines,
                                               DisplayI2c_vprintf,
                                               DisplayI2c_close,
                                               DisplayI2c_control,
                                               DisplayI2c_getType, };

// commands
#define CMD_CLEARDISPLAY   0x01
#define CMD_RETURNHOME     0x02
#define CMD_ENTRYMODESET   0x04
#define CMD_DISPLAYCONTROL 0x08
#define CMD_CURSORSHIFT    0x10
#define CMD_FUNCTIONSET    0x20
#define CMD_SETCGRAMADDR   0x40
#define CMD_SETDDRAMADDR   0x80

// flags for display entry mode
#define LCD_ENTRYLEFT  0x02
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON  0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON   0x02
#define LCD_CURSOROFF  0x00
#define LCD_BLINKON    0x01
#define LCD_BLINKOFF   0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE  0x00
#define LCD_MOVERIGHT   0x04
#define LCD_MOVELEFT    0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE    0x08
#define LCD_1LINE    0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS  0x00

/// These are Bit-Masks for the special signals and background light
#define PCF_RS        0x01
#define PCF_RW        0x02
#define PCF_EN        0x04
#define PCF_BACKLIGHT 0x08

//register select
#define RSMODE_CMD  0
#define RSMODE_DATA 1

void i2cWrite(Display_Handle hDisplay, uint8_t halfByte, uint8_t registerSelect,
              uint8_t enable)
{
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;

    uint8_t i2cData = halfByte << 4;
    if (registerSelect)
        i2cData |= PCF_RS;
    if (enable)
        i2cData |= PCF_EN;
    if (hwAttrs->backlight)
        i2cData |= PCF_BACKLIGHT;

    I2C_Transaction i2cTransaction;
    i2cTransaction.slaveAddress = hwAttrs->address;
    i2cTransaction.writeBuf = &i2cData;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = NULL;
    i2cTransaction.readCount = 0;
    I2C_transfer(object->hI2c, &i2cTransaction);
}

void sendNibble(Display_Handle hDisplay, uint8_t halfByte,
                uint8_t registerSelect)
{
    i2cWrite(hDisplay, halfByte, registerSelect, true);
    usleep(1);
    i2cWrite(hDisplay, halfByte, registerSelect, false);
}

void sendByte(Display_Handle hDisplay, uint8_t value, uint8_t registerSelect)
{
    uint8_t valueLo = value & 0x0F;
    uint8_t valueHi = value >> 4 & 0x0F;

    sendNibble(hDisplay, valueHi, registerSelect);
    sendNibble(hDisplay, valueLo, registerSelect);
}

void command(Display_Handle hDisplay, uint8_t cmd)
{
    sendByte(hDisplay, cmd, RSMODE_CMD);
    if (cmd == CMD_CLEARDISPLAY || cmd == CMD_RETURNHOME)
    {
        usleep(1520);
    }
    else
    {
        usleep(37);
    }
}

void putc(Display_Handle hDisplay, uint8_t character)
{
    sendByte(hDisplay, character, RSMODE_DATA);
}

void moveCursorTo(Display_Handle hDisplay, uint8_t line, uint8_t column)
{
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;

    uint8_t l = line % hwAttrs->numLines;
    uint8_t addr = (l * 0x40 + column) & 0x7f;
    command(hDisplay, CMD_SETDDRAMADDR | addr);
}

void DisplayI2c_init(Display_Handle handle)
{
}

Display_Handle DisplayI2c_open(Display_Handle hDisplay, Display_Params *params)
{
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;

    I2C_Params i2cParams;
    I2C_Params_init(&i2cParams);

    object->mutex = SemaphoreP_createBinary(1);
    if (object->mutex == NULL)
    {
        DebugP_log0("DisplayI2c.c: Couldn't create semaphore");
        return NULL;
    }

    I2C_init();
    object->hI2c = I2C_open(hwAttrs->i2cIdx, &i2cParams);
    if (NULL == object->hI2c)
    {
        DebugP_log0("DisplayI2c.c: Couldn't open I2C");
        return NULL;
    }

    i2cWrite(hDisplay, 0x00, RSMODE_CMD, false);
    usleep(40000);

    //3 kroky a urcite sa dostanem do stavu 8-bit
    sendNibble(hDisplay, 0x3, RSMODE_CMD);
    usleep(4100);
    sendNibble(hDisplay, 0x3, RSMODE_CMD);
    usleep(100);
    sendNibble(hDisplay, 0x3, RSMODE_CMD);
    usleep(37);
    //jeden 8-bit prikaz (spodne 4bity su fixne) ma prepnem na 4-bit
    sendNibble(hDisplay, 0x2, RSMODE_CMD);
    usleep(37);

    uint8_t cmdFunctionSet = CMD_FUNCTIONSET;
    cmdFunctionSet |= LCD_4BITMODE | LCD_5x8DOTS;
    //ked pouzijem iba 1 riadok namiesto dvoch tak vladze svietit aj pri 3.3V
    if (hwAttrs->numLines == 2)
        cmdFunctionSet |= LCD_2LINE;
    command(hDisplay, cmdFunctionSet);

    uint8_t cmd = CMD_DISPLAYCONTROL;
    cmd |= LCD_DISPLAYON;
    if (hwAttrs->cursorOn)
        cmd |= LCD_CURSORON;
    if (hwAttrs->cursorBlink)
        cmd |= LCD_BLINKON;
    command(hDisplay, cmd);

    command(hDisplay, CMD_CLEARDISPLAY);

    uint8_t cmdEntryModeSet = CMD_ENTRYMODESET;
    cmdEntryModeSet |= LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
    command(hDisplay, cmdEntryModeSet);

    return hDisplay;
}

void DisplayI2c_clear(Display_Handle hDisplay)
{
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;

    if (SemaphoreP_pend(object->mutex, hwAttrs->mutexTimeout) == SemaphoreP_OK)
    {
        command(hDisplay, CMD_CLEARDISPLAY);
        SemaphoreP_post(object->mutex);
    }
}

void DisplayI2c_clearLines(Display_Handle hDisplay, uint8_t lineFrom,
                           uint8_t lineTo)
{
    if (lineTo <= lineFrom)
    {
        lineTo = lineFrom;
    }

    //dvojriadkove je 0-0x27
    //jednoriadkove je 0-0x4f
    int l;
    for (l = lineFrom; l <= lineTo; l++)
    {
        moveCursorTo(hDisplay, l, 0);
        int n;
        for (n = 0; n < 0x28; n++)
        {
            //pisem medzere
            putc(hDisplay, 0x20);
        }
    }
}

void DisplayI2c_vprintf(Display_Handle hDisplay, uint8_t line, uint8_t column,
                        char *fmt, va_list va)
{
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;

    uint32_t strSize = 0;

    if (SemaphoreP_pend(object->mutex, hwAttrs->mutexTimeout) == SemaphoreP_OK)
    {
        SystemP_vsnprintf(hwAttrs->strBuf, hwAttrs->strBufLen, fmt, va);

        moveCursorTo(hDisplay, line, column);
        strSize = strlen(hwAttrs->strBuf);

        int i;
        for (i = 0; i < strSize; i++)
        {
            putc(hDisplay, hwAttrs->strBuf[i]);
        }

        SemaphoreP_post(object->mutex);
    }
}

void DisplayI2c_close(Display_Handle hDisplay)
{
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;

    I2C_close(object->hI2c);
    object->hI2c = NULL;

    SemaphoreP_delete(object->mutex);
}

int DisplayI2c_control(Display_Handle hDisplay, unsigned int cmd, void *arg)
{
    DisplayI2c_HWAttrs *hwAttrs = (DisplayI2c_HWAttrs *) hDisplay->hwAttrs;
    DisplayI2c_Object *object = (DisplayI2c_Object *) hDisplay->object;

    switch (cmd)
    {
    case DISPLAY_CMD_TRANSPORT_CLOSE:
        I2C_close(object->hI2c);
        object->hI2c = NULL;
        return DISPLAY_STATUS_SUCCESS;
    case DISPLAY_CMD_TRANSPORT_OPEN:
    {
        I2C_Params i2cParams;
        I2C_Params_init(&i2cParams);
        object->hI2c = I2C_open(hwAttrs->i2cIdx, &i2cParams);
        return DISPLAY_STATUS_SUCCESS;
    }
    }
    return DISPLAY_STATUS_UNDEFINEDCMD;
}

unsigned int DisplayI2c_getType(void)
{
    return Display_Type_INVALID;
}
